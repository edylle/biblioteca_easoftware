package br.ufpa.entity;

import java.util.Date;

/**
 *
 * @author Édylle
 */
public class Reserva {

    private Date horaReserva;
    private Date horaDevolucao;

    public Date getHoraReserva() {
        return horaReserva;
    }

    public void setHoraReserva(Date horaReserva) {
        this.horaReserva = horaReserva;
    }

    public Date getHoraDevolucao() {
        return horaDevolucao;
    }

    public void setHoraDevolucao(Date horaDevolucao) {
        this.horaDevolucao = horaDevolucao;
    }

}
